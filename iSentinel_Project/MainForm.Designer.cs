﻿namespace iSentinel_Project
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.tmrRTLog = new System.Windows.Forms.Timer(this.components);
            this.lbliSentinel = new System.Windows.Forms.Label();
            this.lblPacoCathSchl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblDate
            // 
            this.lblDate.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblDate.BackColor = System.Drawing.Color.Transparent;
            this.lblDate.Font = new System.Drawing.Font("Titillium Web", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.ForeColor = System.Drawing.Color.White;
            this.lblDate.Location = new System.Drawing.Point(70, 409);
            this.lblDate.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(1138, 80);
            this.lblDate.TabIndex = 1;
            this.lblDate.Text = "DAY, MONTH DATE, YEAR";
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTime
            // 
            this.lblTime.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblTime.BackColor = System.Drawing.Color.Transparent;
            this.lblTime.Font = new System.Drawing.Font("Digital-7", 219.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTime.ForeColor = System.Drawing.Color.White;
            this.lblTime.Location = new System.Drawing.Point(-72, 142);
            this.lblTime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(1438, 241);
            this.lblTime.TabIndex = 0;
            this.lblTime.Text = "TIME";
            this.lblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tmrRTLog
            // 
            this.tmrRTLog.Interval = 1000;
            this.tmrRTLog.Tick += new System.EventHandler(this.tmrRTLog_Tick);
            // 
            // lbliSentinel
            // 
            this.lbliSentinel.BackColor = System.Drawing.Color.White;
            this.lbliSentinel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbliSentinel.Font = new System.Drawing.Font("Times New Roman", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbliSentinel.ForeColor = System.Drawing.Color.Black;
            this.lbliSentinel.Image = global::iSentinel_Project.Properties.Resources.elogo2_;
            this.lbliSentinel.Location = new System.Drawing.Point(0, 544);
            this.lbliSentinel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbliSentinel.Name = "lbliSentinel";
            this.lbliSentinel.Size = new System.Drawing.Size(1276, 150);
            this.lbliSentinel.TabIndex = 4;
            this.lbliSentinel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPacoCathSchl
            // 
            this.lblPacoCathSchl.BackColor = System.Drawing.Color.White;
            this.lblPacoCathSchl.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPacoCathSchl.Font = new System.Drawing.Font("Titillium Web", 84.74999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPacoCathSchl.ForeColor = System.Drawing.Color.DimGray;
            this.lblPacoCathSchl.Location = new System.Drawing.Point(0, 0);
            this.lblPacoCathSchl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPacoCathSchl.Name = "lblPacoCathSchl";
            this.lblPacoCathSchl.Size = new System.Drawing.Size(1276, 150);
            this.lblPacoCathSchl.TabIndex = 5;
            this.lblPacoCathSchl.Text = "PACO CATHOLIC SCHOOL";
            this.lblPacoCathSchl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.ForestGreen;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.lblPacoCathSchl);
            this.Controls.Add(this.lbliSentinel);
            this.Controls.Add(this.lblTime);
            this.Controls.Add(this.lblDate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iSentinel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lbliSentinel;
        private System.Windows.Forms.Label lblPacoCathSchl;
        private System.Windows.Forms.Timer tmrRTLog;
    }
}

