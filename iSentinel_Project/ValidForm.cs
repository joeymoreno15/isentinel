﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using DotNetBrowser;
using DotNetBrowser.WinForms;

namespace iSentinel_Project
{
    public partial class ValidForm : Form
    {
        public static int timeTick;

        public ValidForm()
        {
            InitializeComponent();
        }

        private void ValidForm_Load(object sender, EventArgs e)
        {
            Browser browser = BrowserFactory.Create();
            BrowserView browserView = new WinFormsBrowserView(browser);
            Controls.Add((Control)browserView);
            browser.LoadURL(MainForm.url);
            tmrShowForm.Start();
        }

        private void tmrShowForm_Tick(object sender, EventArgs e)
        {
            timeTick += 1;
            if (timeTick >= 2)
            {
                tmrShowForm.Stop(); 
                Browser browser = BrowserFactory.Create();
                BrowserView browserView = new WinFormsBrowserView(browser);
                Controls.Add((Control)browserView);
                browser.LoadURL("");
                this.Hide();
            }
        }
    }
}
