﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Runtime.InteropServices;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using Microsoft.CSharp;
using System.CodeDom.Compiler;

using DotNetBrowser;
using DotNetBrowser.WinForms;

namespace iSentinel_Project
{
    public partial class MainForm : Form
    {
        [DllImport("C:\\Windows\\SysWOW64\\plcommpro.dll", EntryPoint = "Connect")]
        public static extern IntPtr Connect(string Parameters);

        [DllImport("C:\\Windows\\SysWOW64\\plcommpro.dll", EntryPoint = "PullLastError")]
        public static extern int PullLastError();

        [DllImport("C:\\Windows\\SysWOW64\\plcommpro.dll", EntryPoint = "GetRTLog")]
        public static extern int GetRTLog(IntPtr h, ref byte buffer, int buffersize);

        IntPtr h = IntPtr.Zero;

        public static string rfid, type, url;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {            
            string str = "";
            int ret = 0;

            str = "protocol=TCP,ipaddress=192.168.254.201,port=4370,timeout=2000,passwd=";

            lblTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            lblDate.Text = DateTime.Now.ToLongDateString();

            if (IntPtr.Zero == h)
            {
                h = Connect(str);

                if (h != IntPtr.Zero)
                {
                    lblDate.Text = DateTime.Now.ToLongDateString();
                    tmrRTLog.Start();
                }
                else
                {
                    ret = PullLastError();
                    MessageBox.Show("Connect device Failed! The error id is: " + ret);
                    this.Close();
                }
            }
        }

        private void tmrRTLog_Tick(object sender, EventArgs e)
        {
            lblTime.Text = DateTime.Now.ToString("hh:mm:ss tt");
            
            int ret = 0, buffersize = 256;
            string str = "";
            string[] tmp = null;
            byte[] buffer = new byte[256];

            if (IntPtr.Zero != h)
            {
                ret = GetRTLog(h, ref buffer[0], buffersize);
                if (ret >= 0)
                {
                    str = Encoding.Default.GetString(buffer);
                    tmp = str.Split(',');
                    
                    if (tmp[4] == "27")
                    {
                        InvalidForm invalidForm = new InvalidForm();
                        invalidForm.Show();
                        InvalidForm.timeTick = 0;
                    }
                    if (tmp[4] == "0")
                    {
                        if (tmp[3] == "1") type = "login";
                        else type = "logout";

                        rfid = tmp[2];
                        url = "http://localhost/isentinel/admin/gate.php?rfid=" + rfid + "&type=" + type;
                        ValidForm validForm = new ValidForm();
                        validForm.Show();
                        ValidForm.timeTick = 0;
                    }
                }
            }
            else
            {
                MessageBox.Show("Connect device Failed! The error id is: " + ret);
                return;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            InvalidForm invalidForm = new InvalidForm();
            invalidForm.Show();
            InvalidForm.timeTick = 0;
        }
        
    }
}
